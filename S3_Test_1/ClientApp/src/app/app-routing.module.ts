import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestListComponent } from './components/test-list/test-list.component';


const routes: Routes = [
  { path: '', redirectTo: '/testlist', pathMatch: 'full' },
  { path: 'testlist', component: TestListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
