import { Component, Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEventType, HttpResponse, HttpEvent, HttpProgressEvent } from '@angular/common/http';
import { Observable, of, concat } from 'rxjs';
import { delay } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { Spinner } from 'ngx-spinner/lib/ngx-spinner.enum';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'S3 Test 01';
  config: Spinner;
  spinnerMessage: string;

  constructor(private spinner: NgxSpinnerService) {
    this.config = {
      fullScreen: true,
      size: 'medium',
      type: 'timer',
      color: 'white',
    }
  }

  showLoader(message?) {
    this.spinnerMessage = message != null ? message : 'Loading';
    this.spinner.show(undefined, this.config);
  }

  hideLoader() {
    this.spinner.hide();
  }
}

@Injectable()
export class UploadInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url === 'saveUrl') {
      const events: Observable<HttpEvent<any>>[] = [0, 30, 60, 100].map((x) => of(<HttpProgressEvent>{
        type: HttpEventType.UploadProgress,
        loaded: x,
        total: 100
      }).pipe(delay(200)));

      const success = of(new HttpResponse({ status: 200 })).pipe(delay(200));
      events.push(success);

      return concat(...events);
    }

    if (req.url === 'removeUrl') {
      return of(new HttpResponse({ status: 200 }));
    }

    return next.handle(req);
  }
}
