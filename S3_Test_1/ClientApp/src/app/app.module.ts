import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent, UploadInterceptor } from './app.component';
import { TestListComponent } from './components/test-list/test-list.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldErrorDisplayComponent } from './components/field-error-display/field-error-display.component';
import { UploadModule } from '@progress/kendo-angular-upload';
import { FileSaverModule } from 'ngx-filesaver';
import { NgxSpinnerModule } from "ngx-spinner";
import { AlertModule } from 'ngx-alerts';

@NgModule({
  declarations: [
    AppComponent,
    TestListComponent,
    FieldErrorDisplayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgbAlertModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    UploadModule,
    BrowserAnimationsModule,
    FileSaverModule,
    NgxSpinnerModule,
    AlertModule.forRoot({ maxMessages: 5, timeout: 3500, position: 'right' })
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: UploadInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
