import { Component, OnInit } from '@angular/core';
import { Test, TestLanguage } from 'src/app/models/test';
import { HttpCallerService } from 'src/app/services/structure/http-caller.service';
import { ApiRoutes } from 'src/app/constants/api-routes';
import { Trade } from 'src/app/models/trade';
import { Level } from 'src/app/models/level';
import { NgbModalConfig, NgbModal, NgbDateAdapter, NgbDateNativeAdapter } from '@ng-bootstrap/ng-bootstrap';
import { ValidationService } from 'src/app/services/structure/validation.service';
import { Language } from 'src/app/models/language';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import * as _ from "lodash";
import { FileRestrictions, SuccessEvent, FileInfo } from '@progress/kendo-angular-upload';
import { map } from 'rxjs/operators';
import { FileSaverService } from 'ngx-filesaver';
import { AppComponent } from 'src/app/app.component';
import { MessageAlertService } from 'src/app/services/structure/message-alert.service';
import { FileType } from 'src/app/constants/enums';
import { Constants } from 'src/app/constants/constants';

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.css'],
  providers: [{ provide: NgbDateAdapter, useClass: NgbDateNativeAdapter }]
})

export class TestListComponent implements OnInit {
  formModel = this.builder.group({
    TradeId: [0, [Validators.required, Validators.min(1)]],
    LevelId: [0, [Validators.required, Validators.min(1)]],
    SyllabusName: ['', Validators.required],
    DevelopmentOfficer: ['', Validators.required],
    Manager: ['', Validators.required],
    ActiveDate: [new Date(), Validators.required],
    SyllabusFile: [null, [Validators.required]],
    TestPlanFile: [null, [Validators.required]],
    Languages: [null],
    LanguageSelected: [0, [Validators.required, Validators.min(1)]]
  });;
  title = '';
  collectionSize = 0;
  pageNo = 1;
  pageSize = 10;
  tradeId = 0;
  levelId = 0;
  tests: Test[];
  trades: Trade[];
  levels: Level[];
  filterdLevels: Level[];
  filterdChildLevels: Level[];
  languages: Language[];
  dataInfo = '';
  model: Test;
  formSubmitAttempt: boolean;
  isNewTest: boolean;
  uploadSaveUrl = 'saveUrl';
  uploadRemoveUrl = 'removeUrl';
  restrictions: FileRestrictions = {
    allowedExtensions: ['.jpg', '.jpeg', '.gif', '.png', '.doc', '.docx', '.pdf']
  };

  constructor(private service: HttpCallerService, config: NgbModalConfig, private modalService: NgbModal, private validationService: ValidationService, private builder: FormBuilder, private fileSaverService: FileSaverService, private alertService: MessageAlertService, private app: AppComponent) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit() {
    this.getBasicData();
  }

  getLanguageControls() {
    let selected: boolean;
    const arr = this.languages.map(element => {
      if (this.model != null && this.model.Languages != null && this.model.Languages.length > 0) {
        selected = this.model.Languages.filter((lang) => lang.LanguageId == element.LanguageId).length > 0;
        return this.builder.control(selected);
      }
      return this.builder.control(false);
    });
    return new FormArray(arr);
  }

  resetModel() {
    this.model = {
      TestId: 0,
      TradeId: 0,
      LevelId: 0,
      SyllabusName: '',
      DevelopmentOfficer: '',
      Manager: '',
      LanguageCodes: '',
      ActiveDate: new Date(),
      SyllabusFileName: null,
      TestPlanFileName: null,
      SyllabusFileStream: null,
      TestPlanFileStream: null,
      Languages: null
    };
  }

  prepareFormData() {
    let existingSyllabusFile, existingTestPlanFile: FileInfo;
    if (this.model.TestId > 0) {
      existingSyllabusFile = {
        name: this.model.SyllabusFileName
      };
      existingTestPlanFile = {
        name: this.model.TestPlanFileName
      };
    }
    this.formModel = this.builder.group({
      TradeId: [this.model.TradeId, [Validators.required, Validators.min(1)]],
      LevelId: [this.model.LevelId, [Validators.required, Validators.min(1)]],
      SyllabusName: [this.model.SyllabusName, Validators.required],
      DevelopmentOfficer: [this.model.DevelopmentOfficer, Validators.required],
      Manager: [this.model.Manager, Validators.required],
      ActiveDate: [new Date(this.model.ActiveDate), Validators.required],
      SyllabusFile: [this.model.TestId == 0 ? null : Array<FileInfo>(existingSyllabusFile), [Validators.required]],
      TestPlanFile: [this.model.TestId == 0 ? null : Array<FileInfo>(existingTestPlanFile), [Validators.required]],
      Languages: this.getLanguageControls(),
      LanguageSelected: [0, [Validators.required, Validators.min(1)]]
    });
    this.changeLanguageState();
  }

  resetForm() {
    this.title = this.isNewTest ? 'Add Test' : 'Edit Test';
    if (this.formModel != null) {
      this.formModel.reset();
      this.formModel.get('TradeId').setValue(0);
      this.formModel.get('LevelId').setValue(0);
      this.formModel.get('SyllabusFile').setValue(Array<FileInfo>(0));
      this.formModel.get('TestPlanFile').setValue(Array<FileInfo>(0));
    }
    this.model = {
      TradeId: 0,
      LevelId: 0,
      TestId: 0,
      DevelopmentOfficer: '',
      Manager: '',
      ActiveDate: new Date(),
      LanguageCodes: '',
      SyllabusName: '',
      SyllabusFileName: '',
      TestPlanFileName: '',
      SyllabusFileStream: null,
      TestPlanFileStream: null,
      Languages: []
    };
    this.formSubmitAttempt = false;
  }

  get languageArray() {
    return this.formModel.get('Languages');
  }

  getBasicData() {
    this.app.showLoader(Constants.BasicDataLoaderMessage);
    this.getTrades();
  }

  getTrades() {
    this.service.get<Trade[]>(ApiRoutes.Route_BasicData + ApiRoutes.Route_Basic_GetTrades).subscribe(
      res => {
        this.trades = res;
        this.getLevels();
      },
      err => {
        this.alertService.showErrorAlert(err);
      }
    );
  }

  getLevels() {
    this.service.get<Level[]>(ApiRoutes.Route_BasicData + ApiRoutes.Route_Basic_GetLevels).subscribe(
      res => {
        this.levels = res;
        this.getLanguages();
      },
      err => {
        this.alertService.showErrorAlert(err);
      }
    );
  }

  getLanguages() {
    this.service.get<Language[]>(ApiRoutes.Route_BasicData + ApiRoutes.Route_Basic_GetLanguages).subscribe(
      res => {
        this.languages = res;
        this.app.hideLoader();
        this.getTests();
      },
      err => {
        this.alertService.showErrorAlert(err);
      }
    );
  }

  getTests() {
    this.app.showLoader(Constants.TestLoaderMessage);
    this.service.get<number>(ApiRoutes.Route_TestData + ApiRoutes.Route_Test_GetTestCount, 'tradeId=' + this.tradeId + '&levelId=' + this.levelId).subscribe(
      res => {
        this.collectionSize = res;
        this.dataInfo = 'Showing ' + ((this.pageNo - 1) * Math.min(this.collectionSize, this.pageSize) + this.collectionSize == 0 ? 0 : 1) + ' to ' + (this.pageNo * Math.min(this.collectionSize, this.pageSize)) + ' of ' + this.collectionSize + ' entries';
      },
      err => {
        this.alertService.showErrorAlert(err);
      }
    );

    this.service.get<Test[]>(ApiRoutes.Route_TestData + ApiRoutes.Route_Test_GetTests, 'tradeId=' + this.tradeId + '&levelId=' + this.levelId + '&pageNo=' + this.pageNo + '&pageSize=' + this.pageSize).subscribe(
      res => {
        this.tests = res;
        this.app.hideLoader();
      },
      err => {
        this.alertService.showErrorAlert(err);
      }
    );
  }

  getTestById(testId) {
    this.app.showLoader(Constants.GetTestLoaderMessage);
    this.service.get<Test>(ApiRoutes.Route_TestData + ApiRoutes.Route_Test_GetTestById, 'testId=' + testId).subscribe(
      res => {
        if (res != null) {
          this.model = res;
          this.model.ActiveDate = new Date(res.ActiveDate);
          this.childTradeChanged(this.model.LevelId);
        }
        else this.resetModel();
        this.prepareFormData();
        this.app.hideLoader();
      },
      err => {
        this.alertService.showErrorAlert(err);
      }
    );
  }

  showTest(content, testId?) {
    this.resetForm();
    if (testId != null) {
      this.getTestById(testId);
    }
    else {
      this.resetModel();
      this.prepareFormData();
    }
    this.modalService.open(content, { centered: true, size: 'xl' });
  }

  downloadFile(testId, fileName, fileType) {
    this.app.showLoader(Constants.FileDownloadLoaderMessage);
    this.service.getBlob<Blob>(ApiRoutes.Route_TestData + ApiRoutes.Route_Test_DownloadFile, "testId=" + testId + "&fileType=" + fileType).subscribe(
      res => {
        this.fileSaverService.save(res, fileName);
        this.app.hideLoader();
      },
    );
  }

  searchTests() {
    this.pageNo = 1;
    this.getTests();
  }

  onsubmit() {
    if (this.formModel.valid) {
      this.app.showLoader(Constants.SaveTestLoaderMessage);
      this.service.post<any>(ApiRoutes.Route_TestData + ApiRoutes.Route_Test_SaveTest, this.model).subscribe(
        () => {
          if (this.model.TestId == 0) {
            this.resetForm();
          }
          this.alertService.showSuccessAlert(this.model.TestId == 0 ? Constants.TestAddedMessage : Constants.TestUpdatedMessage);
          this.app.hideLoader();
          this.getTests();
        },
        err => {
          this.app.hideLoader();
          this.alertService.showErrorAlert(err);
        }
      );
    }
    else {
      this.validationService.validateAllFormFields(this.formModel);
    }
  }

  successEventHandler(e: SuccessEvent, propType) {
    if (e.operation == 'upload') {
      let file = e.files[0].rawFile;
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = (e) => {
        switch (propType) {
          case FileType.SyllabusFile:
            this.model.SyllabusFileStream = reader.result;
            this.model.SyllabusFileName = file.name;
            break;
          case FileType.TestPlanFile:
            this.model.TestPlanFileStream = reader.result;
            this.model.TestPlanFileName = file.name;
            break;
        }
      }
    }
    else if (e.operation == 'remove') {
      switch (propType) {
        case FileType.SyllabusFile:
          this.model.SyllabusFileStream = null;
          this.model.SyllabusFileName = '';
          break;
        case FileType.TestPlanFile:
          this.model.TestPlanFileStream = null;
          this.model.TestPlanFileName = '';
          break;
      }
    }
  }

  changeLanguageState() {
    let selectedLanguages = _.map(
      this.formModel.controls.Languages["controls"],
      (language, i) => {
        if (language.value == true) {
          let existed = this.model.Languages.filter((lang) => lang.LanguageId == this.languages[i].LanguageId)[0];
          let testLang: TestLanguage = {
            LanguageId: this.languages[i].LanguageId,
            TestId: this.model.TestId,
            TestLanguageId: existed != null ? existed.TestLanguageId : 0
          }
          return testLang;
        }
      }
    ).filter((lang) => lang != null);
    this.model.Languages = selectedLanguages;
    this.formModel.get('LanguageSelected').setValue(selectedLanguages.length);
  }

  tradeChanged() {
    this.levelId = 0;
    this.filterdLevels = this.levels.filter((level) => level.TradeId == this.tradeId);
  }

  childTradeChanged(levelId?) {
    this.model.LevelId = 0;
    this.filterdChildLevels = this.levels.filter((level) => level.TradeId == this.model.TradeId);
    if (levelId != null) this.model.LevelId = levelId;
  }

  isFieldValid(field: string) {
    return this.validationService.isFieldValid(field, this.formModel, this.formSubmitAttempt);
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
}
