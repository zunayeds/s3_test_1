export class ApiRoutes {
  public static Route_BasicData: string = 'api/BasicData/';
  public static Route_Basic_GetTrades: string = 'GetTrades/';
  public static Route_Basic_GetLevels: string = 'GetLevels/';
  public static Route_Basic_GetLanguages: string = 'GetLanguages/';

  public static Route_TestData: string = 'api/TestData/';
  public static Route_Test_GetTests: string = 'GetTests/';
  public static Route_Test_GetTestById: string = 'GetTestById/';
  public static Route_Test_GetTestCount: string = 'GetTestCount/';
  public static Route_Test_SaveTest: string = 'SaveTest/';
  public static Route_Test_DownloadFile: string = 'DownloadFile/';
}
