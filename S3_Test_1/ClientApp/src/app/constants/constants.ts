export class Constants {
  public static readonly BasicDataLoaderMessage: string = 'Loading Basic Data';
  public static readonly TestLoaderMessage: string = 'Loading Tests';
  public static readonly GetTestLoaderMessage: string = 'Getting Test information';
  public static readonly FileDownloadLoaderMessage: string = 'Downloading file';
  public static readonly SaveTestLoaderMessage: string = 'Saving Test information';
  public static readonly TestAddedMessage: string = 'Test Information added Successfully';
  public static readonly TestUpdatedMessage: string = 'Test Information updated Successfully';
}
