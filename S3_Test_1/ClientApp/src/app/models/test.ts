export class Test {
  public TestId: number;
  public TradeId: number;
  public LevelId: number;
  public SyllabusName: string;
  public DevelopmentOfficer: string;
  public Manager: string;
  public LanguageCodes: string;
  public ActiveDate: Date;
  public SyllabusFileName: string;
  public TestPlanFileName: string;
  public SyllabusFileStream: any;
  public TestPlanFileStream: any;
  public Languages: TestLanguage[];
}

export class TestLanguage {
  public TestLanguageId: number;
  public TestId: number;
  public LanguageId: number;
}
