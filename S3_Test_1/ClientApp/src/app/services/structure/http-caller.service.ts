import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpCallerService {
  baseURL: string = '';
  headers: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private httpService: HttpClient) {
  }

  get<T>(url: string, params?: string) {
    return this.httpService.get<T>(this.baseURL + '/' + url + (params != null ? '?' + params : ''), { responseType: 'json', headers: this.headers });
  }

  post<T>(url: string, data: any) {
    return this.httpService.post<T>(this.baseURL + '/' + url, data, { responseType: 'json', headers: this.headers, reportProgress: true });
  }

  getBlob<T>(url: string, params?: string) {
    return this.httpService.get<T>(this.baseURL + '/' + url + (params != null ? '?' + params : ''), { responseType: 'blob' as 'json' });
  }
  
}
