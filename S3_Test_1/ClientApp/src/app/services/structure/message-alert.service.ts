import { Injectable } from '@angular/core';
import { AlertService } from 'ngx-alerts';

@Injectable({
  providedIn: 'root'
})

export class MessageAlertService {
  constructor(private alertService: AlertService) {}

  showSuccessAlert(message) {
    this.alertService.success(message);
  }
  showErrorAlert(message) {
    this.alertService.danger(message);
  }
  showInfoAlert(message) {
    this.alertService.info(message);
  }
  showWarningAlert(message) {
    this.alertService.warning(message);
  }
}
