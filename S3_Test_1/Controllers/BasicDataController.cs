﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using S3_Test_1.Services;
using System;

namespace S3_Test_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasicDataController : ControllerBase
    {
        private readonly ITradeService _tradeService;
        private readonly ILevelService _levelService;
        private readonly ILanguageService _languageService;

        public BasicDataController(ITradeService tradeService, ILevelService levelService, ILanguageService languageService)
        {
            _tradeService = tradeService;
            _levelService = levelService;
            _languageService = languageService;
        }

        [HttpGet]
        [Route("GetTrades")]
        public IActionResult GetTrades()
        {
            try
            {
                return StatusCode(StatusCodes.Status200OK, _tradeService.GetAll());
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetLevels")]
        public IActionResult GetLevels()
        {
            try
            {
                return StatusCode(StatusCodes.Status200OK, _levelService.GetAll());
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        [Route("GetLanguages")]
        public IActionResult GetLanguages()
        {
            try
            {
                return StatusCode(StatusCodes.Status200OK, _languageService.GetAll());
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}