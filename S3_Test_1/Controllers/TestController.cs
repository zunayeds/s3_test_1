﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using S3_Test_1.Helpers;
using S3_Test_1.Models;
using S3_Test_1.Services;
using System;
using System.Linq.Expressions;

namespace S3_Test_1.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TestDataController : ControllerBase
    {
        private readonly ITestService _testService;
        private readonly ITestLanguageService _testLanguageService;

        public TestDataController(ITestService testService, ITestLanguageService testLanguageService)
        {
            _testService = testService;
            _testLanguageService = testLanguageService;
        }

        [HttpGet("{tradeId}/{levelId}/{pageNo}/{pageSize}")]
        [Route("GetTests")]
        public IActionResult GetTests([FromQuery]int tradeId, [FromQuery]int levelId, [FromQuery]int pageNo, [FromQuery]int pageSize)
        {
            try
            {
                Expression<Func<Test, bool>> filter = f => (tradeId == 0 || (tradeId > 0 && f.TradeId == tradeId)) && (levelId == 0 || (levelId > 0 && f.LevelId == levelId));
                return StatusCode(StatusCodes.Status200OK, _testService.Get(filter, page: pageNo, pageSize: pageSize));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("{testId}")]
        [Route("GetTestById")]
        public IActionResult GetTestById([FromQuery]int testId)
        {
            try
            {
                return StatusCode(StatusCodes.Status200OK, _testService.Get(testId));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpGet("{tradeId}/{levelId}")]
        [Route("GetTestCount")]
        public IActionResult GetTestCount([FromQuery]int tradeId, [FromQuery]int levelId)
        {
            try
            {
                return StatusCode(StatusCodes.Status200OK, _testService.GetCount(tradeId, levelId));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("SaveTest")]
        public IActionResult SaveTest([FromBody]JObject obj)
        {
            try
            {
                Test test = obj.ToObject<Test>();
                if (test.TestId == 0)
                    return StatusCode(StatusCodes.Status200OK, _testService.Add(test));
                else return StatusCode(StatusCodes.Status200OK, _testService.Update(test));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        
        [HttpGet("{testId}/{fileType}")]
        [Route("DownloadFile")]
        public IActionResult DownloadFile([FromQuery]int testId, [FromQuery]int fileType)
        {
            var data = _testService.Get(testId);
            if (data != null)
            {
                var fileBytes = fileType == 1 ? data.SyllabusFile : data.TestPlanFile;
                var fileName = fileType == 1 ? data.SyllabusFileName : data.TestPlanFileName;
                return File(fileBytes, fileName.GetFileType(), fileName);
            }
            return null;
        }
    }
}