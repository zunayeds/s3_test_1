﻿using System;

namespace S3_Test_1.Helpers
{
    public static class Extentions
    {
        public static string ConvertToBase64String(this byte[] data)
        {
            return Convert.ToBase64String(data);
        }

        public static byte[] ConvertToByteArray(this string data)
        {
            int commaIndex = data.IndexOf(',');
            if (commaIndex > 1) data = data.Substring(commaIndex + 1);
            return Convert.FromBase64String(data);
        }
        public static string GetFileType(this string fileName)
        {
            int index = fileName.LastIndexOf('.');
            string fileType = string.Empty;
            
            if (index > 0)
            {
                string extension = fileName.Substring(index);
                switch (extension)
                {
                    case ".jpg":
                    case ".jpeg":
                    case ".gif":
                    case ".png":
                        fileType = string.Format("image/{0}", extension.Substring(1));
                        break;
                    case ".doc":
                    case ".docx":
                        fileType = "application/vnd.ms-word";
                        break;
                    case ".pdf":
                        fileType = "application/pdf";
                        break;
                }
            }
            return fileType;
        }
    }
}
