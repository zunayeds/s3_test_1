﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace S3_Test_1.Migrations
{
    public partial class inititalMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Language",
                columns: table => new
                {
                    LanguageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LanguageCode = table.Column<string>(maxLength: 4, nullable: false),
                    LanguageName = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Language", x => x.LanguageId);
                });

            migrationBuilder.CreateTable(
                name: "Test",
                columns: table => new
                {
                    TestId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TradeId = table.Column<int>(nullable: false),
                    LevelId = table.Column<int>(nullable: false),
                    SyllabusName = table.Column<string>(maxLength: 30, nullable: false),
                    DevelopmentOfficer = table.Column<string>(maxLength: 100, nullable: false),
                    Manager = table.Column<string>(maxLength: 100, nullable: false),
                    SyllabusFileName = table.Column<string>(maxLength: 200, nullable: false),
                    TestPlanFileName = table.Column<string>(maxLength: 200, nullable: false),
                    SyllabusFile = table.Column<byte[]>(nullable: false),
                    TestPlanFile = table.Column<byte[]>(nullable: false),
                    ActiveDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<int>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Test", x => x.TestId);
                });

            migrationBuilder.CreateTable(
                name: "Trade",
                columns: table => new
                {
                    TradeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TradeName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trade", x => x.TradeId);
                });

            migrationBuilder.CreateTable(
                name: "TestLanguage",
                columns: table => new
                {
                    TestLanguageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LanguageId = table.Column<int>(nullable: false),
                    TestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TestLanguage", x => x.TestLanguageId);
                    table.ForeignKey(
                        name: "FK_TestLanguage_Test_TestId",
                        column: x => x.TestId,
                        principalTable: "Test",
                        principalColumn: "TestId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Level",
                columns: table => new
                {
                    LevelId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LevelName = table.Column<string>(maxLength: 20, nullable: false),
                    TradeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Level", x => x.LevelId);
                    table.ForeignKey(
                        name: "FK_Level_Trade_TradeId",
                        column: x => x.TradeId,
                        principalTable: "Trade",
                        principalColumn: "TradeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Language",
                columns: new[] { "LanguageId", "LanguageCode", "LanguageName" },
                values: new object[,]
                {
                    { 1, "EN", "English" },
                    { 2, "CH", "Chinese" },
                    { 3, "TH", "Thai" },
                    { 4, "TM", "Tamil" },
                    { 5, "KR", "Korean" },
                    { 6, "BR", "Burmese" }
                });

            migrationBuilder.InsertData(
                table: "Trade",
                columns: new[] { "TradeId", "TradeName" },
                values: new object[,]
                {
                    { 1, "Asphalt Concrete Paving" },
                    { 2, "Bulldozer Operation" },
                    { 3, "Bricklaying" },
                    { 4, "Bored Micro-Piling Operation" }
                });

            migrationBuilder.InsertData(
                table: "Level",
                columns: new[] { "LevelId", "LevelName", "TradeId" },
                values: new object[,]
                {
                    { 1, "SATW", 1 },
                    { 2, "SEC(K)", 1 },
                    { 3, "SATF", 1 },
                    { 4, "SATW", 2 },
                    { 5, "SEC(K)", 2 },
                    { 6, "SEC(K)", 3 },
                    { 7, "SEC(K)", 4 },
                    { 8, "SATW", 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Level_TradeId",
                table: "Level",
                column: "TradeId");

            migrationBuilder.CreateIndex(
                name: "IX_TestLanguage_TestId",
                table: "TestLanguage",
                column: "TestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Language");

            migrationBuilder.DropTable(
                name: "Level");

            migrationBuilder.DropTable(
                name: "TestLanguage");

            migrationBuilder.DropTable(
                name: "Trade");

            migrationBuilder.DropTable(
                name: "Test");
        }
    }
}
