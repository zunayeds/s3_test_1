﻿using Microsoft.EntityFrameworkCore;

namespace S3_Test_1.Models.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Trade> Trades { get; set; }
        public DbSet<Level> Levels { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestLanguage> TestLanguages { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            int seedId = 1;
            Trade[] _trades = new Trade[]
            {
                new Trade()
                {
                    TradeId = seedId++,
                    TradeName = "Asphalt Concrete Paving"
                },
                new Trade()
                {
                    TradeId = seedId++,
                    TradeName = "Bulldozer Operation"
                },
                new Trade()
                {
                    TradeId = seedId++,
                    TradeName = "Bricklaying"
                },
                new Trade()
                {
                    TradeId = seedId++,
                    TradeName = "Bored Micro-Piling Operation"
                }
            };

            seedId = 1;
            Language[] _languages = new Language[]
            {
                new Language() { LanguageId = seedId++, LanguageCode = "EN", LanguageName = "English" },
                new Language() { LanguageId = seedId++, LanguageCode = "CH", LanguageName = "Chinese" },
                new Language() { LanguageId = seedId++, LanguageCode = "TH", LanguageName = "Thai" },
                new Language() { LanguageId = seedId++, LanguageCode = "TM", LanguageName = "Tamil" },
                new Language() { LanguageId = seedId++, LanguageCode = "KR", LanguageName = "Korean" },
                new Language() { LanguageId = seedId++, LanguageCode = "BR", LanguageName = "Burmese" }
            };

            seedId = 1;
            Level[] _levels = new Level[]
            {
                new Level() { LevelId = seedId++, TradeId = 1, LevelName = "SATW" },
                new Level() { LevelId = seedId++, TradeId = 1, LevelName = "SEC(K)" },
                new Level() { LevelId = seedId++, TradeId = 1, LevelName = "SATF" },
                new Level() { LevelId = seedId++, TradeId = 2, LevelName = "SATW" },
                new Level() { LevelId = seedId++, TradeId = 2, LevelName = "SEC(K)" },
                new Level() { LevelId = seedId++, TradeId = 3, LevelName = "SEC(K)" },
                new Level() { LevelId = seedId++, TradeId = 4, LevelName = "SEC(K)" },
                new Level() { LevelId = seedId++, TradeId = 4, LevelName = "SATW" },
            };

            modelBuilder.Entity<Language>().HasData(_languages);
            modelBuilder.Entity<Trade>().HasData(_trades);
            modelBuilder.Entity<Level>(entity =>
            {
                entity.HasOne(d => d.Trade)
                    .WithMany(p => p.Levels)
                    .HasForeignKey("TradeId");
            });
            modelBuilder.Entity<Level>().HasData(_levels);
                
        }
    }
}