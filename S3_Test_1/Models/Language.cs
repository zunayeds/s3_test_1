﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace S3_Test_1.Models
{
    [Table("Language")]
    public class Language
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LanguageId { get; set; }

        [Required]
        [MaxLength(4)]
        public string LanguageCode { get; set; }

        [Required]
        [MaxLength(20)]
        public string LanguageName { get; set; }
    }
}
