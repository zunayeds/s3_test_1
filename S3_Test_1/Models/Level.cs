﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace S3_Test_1.Models
{
    [Table("Level")]
    public class Level
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LevelId { get; set; }

        [Required]
        [MaxLength(20)]
        public string LevelName { get; set; }

        [Required]
        [ForeignKey("TradeId")]
        public int TradeId { get; set; }

        public Trade Trade { get; set; }
    }
}
