﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace S3_Test_1.Models
{
    [Table("Test")]
    public class Test
    {
        public Test()
        {
            CreatedBy = 0;
            CreatedDate = DateTime.Now;
            SyllabusFileName = string.Empty;
            TestPlanFileName = string.Empty;
            IsActive = true;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TestId { get; set; }

        [Required]
        public int TradeId { get; set; }

        [Required]
        [ForeignKey("LevelId")]
        public int LevelId { get; set; }

        [Required]
        [MaxLength(30)]
        public string SyllabusName { get; set; }

        [Required]
        [MaxLength(100)]
        public string DevelopmentOfficer { get; set; }

        [Required]
        [MaxLength(100)]
        public string Manager { get; set; }

        [Required]
        [MaxLength(200)]
        public string SyllabusFileName { get; set; }

        [Required]
        [MaxLength(200)]
        public string TestPlanFileName { get; set; }

        [Required]
        public byte[] SyllabusFile { get; set; }

        [Required]
        public byte[] TestPlanFile { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ActiveDate { get; set; }

        [Required]
        public int CreatedBy { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime CreatedDate { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [DefaultValue(true)]
        public bool IsActive { get; set; }

        public virtual ICollection<TestLanguage> Languages { get; set; }


        [NotMapped]
        public string LanguageCodes { get; set; }
        [NotMapped]
        public string TradeName { get; set; }
        [NotMapped]
        public string LevelName { get; set; }
        [NotMapped]
        public string UserName { get; set; }
        [NotMapped]
        public string SyllabusFileStream { get; set; }
        [NotMapped]
        public string TestPlanFileStream { get; set; }
    }
}
