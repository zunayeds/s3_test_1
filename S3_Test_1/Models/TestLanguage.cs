﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace S3_Test_1.Models
{
    [Table("TestLanguage")]
    public class TestLanguage
    {
        [Key]
        public int TestLanguageId { get; set; }

        [Required]
        [ForeignKey("LanguageId")]
        public int LanguageId { get; set; }

        [Required]
        [ForeignKey("TestId")]
        public int TestId { get; set; }
    }
}
