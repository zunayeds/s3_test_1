﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace S3_Test_1.Models
{
    [Table("Trade")]
    public class Trade
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TradeId { get; set; }

        [Required]
        [MaxLength(50)]
        public string TradeName { get; set; }

        public virtual ICollection<Level> Levels { get; set; }
    }
}
