﻿using S3_Test_1.Models;
using S3_Test_1.Models.Repository;

namespace S3_Test_1.Services
{
    public interface ILanguageService : IDataRepository<Language>
    {
        
    }
}
