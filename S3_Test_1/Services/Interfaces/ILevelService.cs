﻿using S3_Test_1.Models;
using S3_Test_1.Models.Repository;
using System.Collections.Generic;

namespace S3_Test_1.Services
{
    public interface ILevelService : IDataRepository<Level>
    {
        IEnumerable<Level> GetByTradeId(int tradeId);
    }
}
