﻿using S3_Test_1.Models;
using S3_Test_1.Models.Repository;

namespace S3_Test_1.Services
{
    public interface ITestService : IDataRepository<Test>
    {
        int GetCount(int tradeId, int levelId);
    }
}
