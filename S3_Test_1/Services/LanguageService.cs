﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using S3_Test_1.Models;
using S3_Test_1.Models.Context;

namespace S3_Test_1.Services
{
    public class LanguageService : ILanguageService
    {
        readonly DataContext _context;
        public LanguageService(DataContext context)
        {
            _context = context;
        }
        public int Add(Language entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Language entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Language> Get(Expression<Func<Language, bool>> filter = null, string[] includePaths = null, int? page = 0, int? pageSize = null)
        {
            throw new NotImplementedException();
        }

        public Language Get(int id)
        {
            return _context.Languages.Where(w => w.LanguageId == id).FirstOrDefault();
        }

        public IEnumerable<Language> GetAll()
        {
            return _context.Languages.ToList();
        }

        public bool Update(Language entity)
        {
            throw new NotImplementedException();
        }
    }
}
