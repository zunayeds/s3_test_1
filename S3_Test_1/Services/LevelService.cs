﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using S3_Test_1.Models;
using S3_Test_1.Models.Context;

namespace S3_Test_1.Services
{
    public class LevelService : ILevelService
    {
        readonly DataContext _context;
        public LevelService(DataContext context)
        {
            _context = context;
        }
        public int Add(Level entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Level entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Level> Get(Expression<Func<Level, bool>> filter = null, string[] includePaths = null, int? page = 0, int? pageSize = null)
        {
            throw new NotImplementedException();
        }

        public Level Get(int id)
        {
            return _context.Levels.Where(w => w.LevelId == id).FirstOrDefault();
        }

        public IEnumerable<Level> GetByTradeId(int tradeId)
        {
            return _context.Levels.Where(w => w.TradeId == tradeId);
        }

        public IEnumerable<Level> GetAll()
        {
            return _context.Levels.ToList();
        }

        public bool Update(Level entity)
        {
            throw new NotImplementedException();
        }
    }
}
