﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using S3_Test_1.Models;
using S3_Test_1.Models.Context;

namespace S3_Test_1.Services
{
    public class TestLanguageService : ITestLanguageService
    {
        readonly DataContext _context;
        private DbSet<TestLanguage> _dbSet;
        public TestLanguageService(DataContext context)
        {
            _context = context;
            _dbSet = _context.Set<TestLanguage>();
        }
        public int Add(TestLanguage entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(TestLanguage entity)
        {
            _context.Entry(entity).State = EntityState.Deleted;
            _context.SaveChanges();
            return true;
        }

        public IEnumerable<TestLanguage> Get(Expression<Func<TestLanguage, bool>> filter = null, string[] includePaths = null, int? page = 0, int? pageSize = null)
        {
            IQueryable<TestLanguage> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includePaths != null)
            {
                for (var i = 0; i < includePaths.Count(); i++)
                {
                    query = query.Include(includePaths[i]);
                }
            }

            if (pageSize != null)
            {
                query = query.Skip((int)pageSize * ((int)page - 1)).Take((int)pageSize);
            }

            return query.ToList();
        }

        public TestLanguage Get(int id)
        {
            return _context.TestLanguages.Where(w => w.TestLanguageId == id).FirstOrDefault();
        }

        public IEnumerable<TestLanguage> GetAll()
        {
            return _context.TestLanguages.ToList();
        }

        public bool Update(TestLanguage entity)
        {
            throw new NotImplementedException();
        }
    }
}
