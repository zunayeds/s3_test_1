﻿using Microsoft.EntityFrameworkCore;
using S3_Test_1.Helpers;
using S3_Test_1.Models;
using S3_Test_1.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace S3_Test_1.Services
{
    public class TestService : ITestService
    {
        readonly DataContext _context;
        readonly ITestLanguageService _testLanguageService;
        private DbSet<Test> _dbSet;
        public TestService(DataContext context, ITestLanguageService testLanguageService)
        {
            _context = context;
            _dbSet = _context.Set<Test>();
            _testLanguageService = testLanguageService;
        }
        public int Add(Test entity)
        {
            entity.SyllabusFile = entity.SyllabusFileStream.ConvertToByteArray();
            entity.TestPlanFile = entity.TestPlanFileStream.ConvertToByteArray();

            foreach (TestLanguage language in entity.Languages)
            {
                _context.Entry(language).State = EntityState.Added;
            }
            _context.Entry(entity).State = EntityState.Added;
            _context.SaveChanges();
            return entity.TestId;
        }

        public bool Delete(Test entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Test> Get(Expression<Func<Test, bool>> filter = null, string[] includePaths = null, int? page = 0, int? pageSize = null)
        {
            IQueryable<Test> query = _dbSet;
            query = from test in _context.Tests
                    join trade in _context.Trades on test.TradeId equals trade.TradeId
                    join level in _context.Levels on test.LevelId equals level.LevelId
                    select new Test
                    {
                        TestId = test.TestId,
                        TradeName = trade.TradeName,
                        LevelName = level.LevelName,
                        SyllabusName = test.SyllabusName,
                        SyllabusFileName = test.SyllabusFileName,
                        TestPlanFileName = test.TestPlanFileName,
                        ActiveDate = test.ActiveDate,
                        LanguageCodes = string.Join(", ", (from testLanguage in _context.TestLanguages
                                                           join language in _context.Languages on testLanguage.LanguageId equals language.LanguageId
                                                           where testLanguage.TestId == test.TestId
                                                           select (language.LanguageCode)))
                    };

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includePaths != null)
            {
                for (var i = 0; i < includePaths.Count(); i++)
                {
                    query = query.Include(includePaths[i]);
                }
            }

            if (pageSize != null)
            {
                query = query.Skip((int)pageSize * ((int)page - 1)).Take((int)pageSize);
            }

            return query.ToList();
        }

        public Test Get(int id)
        {
            var data = _context.Tests.Where(w => w.TestId == id).FirstOrDefault();
            if (data != null)
            {
                data.SyllabusFileStream = data.SyllabusFile.ConvertToBase64String();
                data.TestPlanFileStream = data.TestPlanFile.ConvertToBase64String();
                data.Languages = _context.TestLanguages.Where(w => w.TestId == data.TestId).ToList();
            }
            return data;
        }
        public int GetCount(int tradeId, int levelId)
        {
            return _context.Tests.Count(c => ((tradeId == 0 || c.TradeId == tradeId) && (levelId == 0 || c.LevelId == levelId)));
        }

        public IEnumerable<Test> GetAll()
        {
            return _context.Tests.ToList();
        }

        public bool Update(Test entity)
        {
            entity.SyllabusFile = entity.SyllabusFileStream.ConvertToByteArray();
            entity.TestPlanFile = entity.TestPlanFileStream.ConvertToByteArray();

            Expression<Func<TestLanguage, bool>> filter = f => f.TestId == entity.TestId;
            IEnumerable<TestLanguage> languages = _testLanguageService.Get(filter);

            foreach (TestLanguage language in languages.Where(w => !entity.Languages.Any(l => l.LanguageId == w.LanguageId)))
            {
                _context.Entry(language).State = EntityState.Deleted;
            }
            foreach (TestLanguage language in entity.Languages.Where(w => !languages.Any(l => l.LanguageId == w.LanguageId)))
            {
                _context.Entry(language).State = EntityState.Added;
            }
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
            return true;
        }
    }
}
