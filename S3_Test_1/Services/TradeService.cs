﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using S3_Test_1.Models;
using S3_Test_1.Models.Context;

namespace S3_Test_1.Services
{
    public class TradeService : ITradeService
    {
        readonly DataContext _context;
        public TradeService(DataContext context)
        {
            _context = context;
        }
        public int Add(Trade entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Trade entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Trade> Get(Expression<Func<Trade, bool>> filter = null, string[] includePaths = null, int? page = 0, int? pageSize = null)
        {
            throw new NotImplementedException();
        }

        public Trade Get(int id)
        {
            return _context.Trades.Where(w => w.TradeId == id).FirstOrDefault();
        }

        public IEnumerable<Trade> GetAll()
        {
            return _context.Trades.ToList();
        }

        public bool Update(Trade entity)
        {
            throw new NotImplementedException();
        }
    }
}
