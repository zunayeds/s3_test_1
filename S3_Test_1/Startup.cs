﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using S3_Test_1.Models;
using S3_Test_1.Models.Context;
using S3_Test_1.Models.Repository;
using S3_Test_1.Services;

namespace S3_Test_1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(opts => opts.UseSqlServer(Configuration["ConnectionString:DBConnection"]));
            services.AddTransient<ITradeService, TradeService>();
            services.AddTransient<ILevelService, LevelService>();
            services.AddTransient<ILanguageService, LanguageService>();
            services.AddTransient<ITestService, TestService>();
            services.AddTransient<ITestLanguageService, TestLanguageService>();
            services.AddScoped<IDataRepository<Trade>, TradeService>();
            services.AddScoped<IDataRepository<Level>, LevelService>();
            services.AddScoped<IDataRepository<Language>, LanguageService>();
            services.AddScoped<IDataRepository<Test>, TestService>();
            services.AddScoped<IDataRepository<TestLanguage>, TestLanguageService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options =>
                {
                    var resolver = options.SerializerSettings.ContractResolver;
                    if (resolver != null)
                    {
                        (resolver as DefaultContractResolver).NamingStrategy = null;
                    }
                });
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(options =>
                options.WithOrigins("http://localhost:4200/")
                .AllowAnyMethod()
                .AllowAnyHeader()
            );

            app.UseMvc();
        }
    }
}
